#ifndef MOTORS_H
#define MOTORS_H


enum DIRECTION
{
    FORWARD=0,
    BACKWARD,
    STOP
};

void initMotorsGPIO();
void setDirectionM1(enum DIRECTION dir);
void setDirectionM2(enum DIRECTION dir);
void setDirectionM3(enum DIRECTION dir);
void setDirectionM4(enum DIRECTION dir);


void setPWM(int m1, int m2, int m3, int m4);
void tickPWM(int val);


#endif /* MOTORS_H */

