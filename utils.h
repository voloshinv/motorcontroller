#ifndef UTILS
#define UTILS

// check if symbol in bounds
#define IS_IN_BOUNDS(s, l, h)   ((l <= s) && (s <= h))

unsigned char hexToBin(char c);

#endif
