#include "utils.h"
#include <stdbool.h>

unsigned char hexToBin(char symbol)
{
  if (IS_IN_BOUNDS(symbol, '0', '9'))
      return symbol - '0';
  else if (IS_IN_BOUNDS(symbol, 'a', 'f'))
      return symbol - 'a' + 10;

  return -1;
}
