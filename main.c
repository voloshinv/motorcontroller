#include <avr/io.h>
#include <stdbool.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "usart.h"
#include "motors.h"
#include "utils.h"
#include <ctype.h>
#include <stdlib.h>

#define TIMER0_OVFS_PERSEC ( 1000 / 8 )

uint8_t Timer0_ovfCounter = 0;
ISR (TIMER0_OVF_vect)  // timer0 overflow interrupt
{
    // event to be executed every 8ms here
    Timer0_ovfCounter++;
}

void motorCtrl(unsigned char motor_mask, enum DIRECTION dir)
{
    if (motor_mask & 1)
        setDirectionM1(dir);
    if (motor_mask & 2)
        setDirectionM2(dir);
    if (motor_mask & 4)
        setDirectionM3(dir);
    if (motor_mask & 8)
        setDirectionM4(dir);
}

void parseCmdSymbol(unsigned char symbol)
{
    static int cmd_index = 0;
    static unsigned char motor_mask = 0;

    symbol = tolower(symbol);

    if (symbol == 'm'){
        cmd_index = 1;
        return;
    }

    if (1 == cmd_index)
    {
        motor_mask = hexToBin(symbol);
        cmd_index = (motor_mask != -1) ? 2 : 0;
        return;
    }

    if (2 == cmd_index)
    {
        switch(symbol)
        {
            case 'f':
                motorCtrl(motor_mask, FORWARD);
                break;
            case 'b':
                motorCtrl(motor_mask, BACKWARD);
                break;
            case 's':
                motorCtrl(motor_mask, STOP);
                break;
        }

        cmd_index = 0;
    }
}

static int speed = 255;

void goLeft(enum DIRECTION dir)
{
    motorCtrl(0xF, dir);
    int lowspeed = speed / 4;
    setPWM(speed, lowspeed, speed, lowspeed);
}

void goRight(enum DIRECTION dir)
{
    motorCtrl(0xF, dir);
    int lowspeed = speed / 4;
    setPWM(lowspeed, speed, lowspeed, speed);
}

void bluetoothRC_Controller(unsigned char symbol)
{
    motorCtrl(0xF, STOP);

    static enum DIRECTION oldDir = STOP;

    switch (symbol)
    {
        case 'F':
            motorCtrl(0xF, FORWARD);
            setPWM(speed, speed, speed, speed);
            oldDir = FORWARD;
            return;
        case 'B':
            motorCtrl(0xF, BACKWARD);
            setPWM(speed, speed, speed, speed);
            oldDir = BACKWARD;
            return;
        case 'L':
            if (STOP != oldDir)
                goLeft(oldDir);
            return;
        case 'R':
            if (STOP != oldDir)
                goRight(oldDir);
            return;
        case 'G':
            goLeft(FORWARD);
            oldDir = FORWARD;
            return;
        case 'I':
            goRight(FORWARD);
            oldDir = FORWARD;
            return;
        case 'H':
            goLeft(BACKWARD);
            oldDir = BACKWARD;
            return;
        case 'J':
            goRight(BACKWARD);
            oldDir = BACKWARD;
            return;
        case 'S':
        case 'D':
            motorCtrl(0xF, STOP);
            oldDir = STOP;
            return;
        case 'q':
            speed = 255;
            return;
    }

    if (IS_IN_BOUNDS(symbol, '0', '9')){
        speed = symbol - '0';
        speed *= 25;
    }
}

void Timer0_Init(void)
{
    TIMSK |= (1 << TOIE0); // timer0 overflow interrupt
    TCCR0 = (1<<CS02); // set prescaler to 256 and start the timer
    TCNT0 = 0;
}

void ADC_Init(void)
{
    ADCSRA = (1<<ADEN) | (1<<ADPS2) | (1<<ADPS0);
    // ADEN: Set to turn on ADC , by default it is turned off
    //ADPS2: ADPS2 and ADPS0 set to make division factor 32
    ADMUX=0x05; // ADC input channel set to PC5
}

int main()
{
    USART0_Init(&bluetoothRC_Controller);
    Timer0_Init();
    initMotorsGPIO();
    ADC_Init();

    sei(); // enable interrupts

    USART0_TransmitString("MOTORCTRL module ver.0.1");

    //setPWM(50, 50, 50, 50);   

    ADCSRA |= (1<<ADSC); // Start conversion

    for(;;)
    {
        tickPWM(TCNT0);

        if (Timer0_ovfCounter > TIMER0_OVFS_PERSEC)
        {
            while (ADCSRA & (1<<ADSC)); // wait for conversion to complete
            unsigned int val = ADCW;

            char str[sizeof(int)*8+1];
            itoa(val, str, 10);

            USART0_TransmitString(str);
            USART0_TransmitString("\r\n");

            ADCSRA |= (1<<ADSC); // Start conversion

            Timer0_ovfCounter = 0;
        }
    }

    return 0;
}
