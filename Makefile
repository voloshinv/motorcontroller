#
#   Motor controller project for my super robot
#
CC= avr-gcc

MCU=atmega8

CFLAGS=  -O -g -Wall -ffreestanding -mmcu=$(MCU) -DF_CPU=8000000UL

.SUFFIXES: .s .bin .out .hex .srec

.c.s:
	$(CC) $(CFLAGS) -S $<

.S.o:
	$(CC) $(ASFLAGS) -c $<

.o.out:
	$(CC) $(CFLAGS) -o $@ $<

.out.bin:
	avr-objcopy -O binary $< $@

.out.hex:
	avr-objcopy -O ihex $< $@

.out.srec:
	avr-objcopy -O srec $< $@

all:	motorController.hex

OBJS=main.o usart.o motors.o utils.o
motorController.hex: $(OBJS)
	$(CC) -o motorController.elf $(CFLAGS) $(LDFLAGS) $(OBJS) $(LDLIBS)
	avr-objcopy -j .text -j .data -O ihex motorController.elf motorController.hex

clean:
	rm -f *~ *.out *.bin *.hex *.srec *.s *.o *.pdf *core
