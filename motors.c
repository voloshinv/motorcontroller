#include "motors.h"
#include <avr/io.h>

#define SETPIN(PORT, PIN) { PORT |= 1 << PIN; }
#define RESETPIN(PORT, PIN) { PORT &= ~(1 << PIN); }

void initMotorsGPIO()
{
    DDRB |= 0b11111110; //PINS: 1, 2, 3, 4, 5, 6, 7
    DDRC |= 0b00001111; //PINS: 0, 1, 2, 3
    DDRD |= 0b00010000; //PINS: 4
    
    // set all pins to disabed state
    PORTB &= ~0b11111110;
    PORTC &= ~0b00001111;
    PORTD &= ~0b00010000;
    
    SETPIN(DDRB, PIN3); //SETPIN(PORTB, PIN3);
    SETPIN(DDRB, PIN1); //SETPIN(PORTB, PIN1);
    SETPIN(DDRB, PIN2); //SETPIN(PORTB, PIN2);
    SETPIN(DDRB, PIN5); //SETPIN(PORTB, PIN5);
}

void setDirectionM1(enum DIRECTION dir)
{
    switch(dir)
    {
        case FORWARD:
            RESETPIN(PORTC, PIN1);
            SETPIN(PORTC, PIN0);
            break;
        case BACKWARD:
            RESETPIN(PORTC, PIN0);
            SETPIN(PORTC, PIN1);
            break;
        case STOP:
            RESETPIN(PORTC, PIN0);
            RESETPIN(PORTC, PIN1);
            break;
    }
}

void setDirectionM2(enum DIRECTION dir)
{
    switch(dir)
    {
        case FORWARD:
            RESETPIN(PORTC, PIN2);
            SETPIN(PORTC, PIN3);
            break;
        case BACKWARD:
            RESETPIN(PORTC, PIN3);
            SETPIN(PORTC, PIN2);
            break;
        case STOP:
            RESETPIN(PORTC, PIN3);
            RESETPIN(PORTC, PIN2);
            break;
    }
}

void setDirectionM3(enum DIRECTION dir)
{
    switch(dir)
    {
        case FORWARD:
            RESETPIN(PORTB, PIN7);
            SETPIN(PORTB, PIN4);
            break;
        case BACKWARD:
            RESETPIN(PORTB, PIN4);
            SETPIN(PORTB, PIN7);
            break;
        case STOP:
            RESETPIN(PORTB, PIN4);
            RESETPIN(PORTB, PIN7);
            break;
    }
}

void setDirectionM4(enum DIRECTION dir)
{
    switch(dir)
    {
        case FORWARD:
            RESETPIN(PORTB, PIN6);
            SETPIN(PORTD, PIN4);
            break;
        case BACKWARD:
            RESETPIN(PORTD, PIN4);
            SETPIN(PORTB, PIN6);
            break;
        case STOP:
            RESETPIN(PORTD, PIN4);
            RESETPIN(PORTB, PIN6);
            break;
    }
}

int m1 = 256, m2 = 256, m3 = 256, m4 = 256;
void setPWM(int _m1, int _m2, int _m3, int _m4)
{
    m1 = _m1;
    m2 = _m2;
    m3 = _m3;
    m4 = _m4;
}

void tickPWM(int val)
{
    if (m1 >= val){
        SETPIN(PORTB, PIN3);
    }
    else{
        RESETPIN(PORTB, PIN3);
    }
    
    if (m2 >= val){
        SETPIN(PORTB, PIN1);
    }
    else{
        RESETPIN(PORTB, PIN1);
    }
    
    if (m3 >= val){
        SETPIN(PORTB, PIN2);
    }
    else{
        RESETPIN(PORTB, PIN2);
    }
    
    if (m4 >= val){
        SETPIN(PORTB, PIN5);
    }
    else{
        RESETPIN(PORTB, PIN5);
    }
}
