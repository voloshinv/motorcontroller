#include "usart.h"
#include <avr/io.h>
#include <avr/interrupt.h>

//static unsigned char USART0_InBuf[USART0_InBufSize];
//static volatile unsigned char USART0_InBufInPos = 0;
//static volatile unsigned char USART0_InBufOutPos = 0;
//
//ISR(USART_RXC_vect) // USART, Rx Complete
//{
//    USART0_InBuf[USART0_InBufInPos++] = UDR;
//    if (USART0_InBufInPos >= USART0_InBufSize) USART0_InBufInPos = 0;
//}

void dummyHandler(unsigned char s)
{
    //do nothing
}

static USART_RcvHandler rcvHandler = &dummyHandler;

ISR(USART_RXC_vect) // USART, Rx Complete
{
    rcvHandler(UDR);
}

// --------------
// --- USART0 ---
// --------------

void USART0_Transmit(unsigned char data)
{
    while (!(UCSRA & (1<<UDRE))) {}
    UDR = data;
}

void USART0_TransmitString(char *data)
{
    while (*data) USART0_Transmit(*data++);
}

//unsigned char USART0_Receive(void)
//{
//    unsigned char c;
//    while (USART0_InBufInPos == USART0_InBufOutPos) {}
//    c = USART0_InBuf[USART0_InBufOutPos++];
//    if (USART0_InBufOutPos >= USART0_InBufSize) USART0_InBufOutPos = 0;
//    return c;
//}

void USART0_Init(USART_RcvHandler h)
{
    rcvHandler = h;
    
    // USART0 settings: 38400 baud 8-n-1
    // WARNING: real baud = 38461: err = 0.158854166666678%
    UBRRH = 0;
    UBRRL = 12;
    UCSRB = (1<<RXCIE) | (1<<RXEN) | (1<<TXEN);
    UCSRC = (1<<URSEL) | (1<<UCSZ1) | (1<<UCSZ0);
}
