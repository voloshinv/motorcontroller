#ifndef USART_H
#define USART_H

//#define USART0_InBufSize 20



void USART0_Transmit(unsigned char data);
void USART0_TransmitString(char *data);
//unsigned char USART0_Receive(void);

typedef void(*USART_RcvHandler)(unsigned char);

void USART0_Init(USART_RcvHandler);


#endif /* USART_H */

